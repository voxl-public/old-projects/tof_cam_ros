# Compile

```
cd src/
git clone --depth 1 git@gitlab.modalai.com:modalai/package/tof_cam_ros.git
# Fix to get the TOF include files
echo "Copying TOF include files to system include directory"
cd tof_cam_ros/inc
cp -r royale/ /usr/include
roscd
cd ../
echo "PWD"
pwd

# Run the build
echo "Building project"
source devel/setup.bash
catkin_make -DCMAKE_BUILD_TYPE=Release -DQC_SOC_TARGET=APQ8096 -Wno-dev install
```

# Visualize

* Add the following Topics:
 * LaserScan (/scan) 
 * PointCloud2 (/points)

* The 2 transforms (for both laser scan and point cloud) are in the tof.launch file. 
* In RViz choose base_link as the fixed frame.