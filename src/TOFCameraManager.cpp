/****************************************************************************
 *   Copyright (c) 2017 Michael Shomin. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name ATLFlight nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#include "TOFCameraManager.hpp"

#include <fstream>
#include <iostream>
#include <string>
#include <chrono>

/*TOF Configuration is fixed*/
#define TOF_IRS_WIDTH 224
#define TOF_IRS_HEIGHT 172

TOF::CameraManager::CameraManager(ros::NodeHandle nh, ros::NodeHandle pnh)
    : nh_(nh),
        pnh_(pnh),
        im_trans_(nh_),
        recon_server_(pnh_)
{
    initialized_ = false;
    running_     = false;
    preview_params_set_   = false;
    camera_ptr_           = NULL;
    timestamp_last_nsecs_ = 0;
}


void TOF::CameraManager::advertiseTopics()
{
    if(ir_image_mode_ || publish_ir_image_)
        ir_pub_ = im_trans_.advertiseCamera("ir/image_raw",DEFAULT_DEPTH_IMAGE_QUEUE_SIZE,false);
    cam_pub_ = im_trans_.advertiseCamera("image_raw",DEFAULT_DEPTH_IMAGE_QUEUE_SIZE,false);
    pubPointCloud_ = nh_.advertise<sensor_msgs::PointCloud2>("points", DEFAULT_POINT_CLOUD_QUEUE_SIZE);
    pubLaserScan_ = nh_.advertise<sensor_msgs::LaserScan>("scan", DEFAULT_LASER_SCAN_QUEUE_SIZE);
}

int32_t TOF::CameraManager::Initialize()
{
    if(!initialized_){

        int format, cam_type, width, height, frame_rate, cam_id;

        // parameters not in launch file that should probably be constants
        pnh_.param<int>("format", format, 0); //default to RAW
        pnh_.param<int>("camera_number", cam_type, 1);
        pnh_.param<int>("width", width, TOF_IRS_WIDTH);
        pnh_.param<int>("height", height, TOF_IRS_HEIGHT);

        // parameters exposed in default launch file
        pnh_.param<int>("camera_id", cam_id, TOF::DEFAULT_CAM_ID);
        pnh_.param<int>("frame_rate", frame_rate, 5);
        pnh_.param<int>("auto_exposure", auto_exposure_, 0);

        // confidence should be between 0 and 255
        pnh_.param<int>("confidence_cutoff", confidence_cutoff_, 50);
        pnh_.param<float>("time_offset_s", time_offset_s_, 0.0);
        pnh_.param<float>("min_depth_m", min_depth_m_, 0.05);

        pnh_.param<int>("scan_width_degrees", scan_width_degrees_, 96);
        pnh_.param<int>("laser_scan_line", laser_scan_line_, (TOF_IRS_HEIGHT / 2));
        pnh_.param<bool>("ir_image_mode", ir_image_mode_, false);
        pnh_.param<bool>("publish_ir_image", publish_ir_image_, true);
        pnh_.param<bool>("publish_point_cloud", publish_point_cloud_, true);
        pnh_.param<bool>("publish_depth_image", publish_depth_image_, true);
        pnh_.param<bool>("publish_laser_scan", publish_laser_scan_, true);
        pnh_.param<bool>("flip_image", flip_image_, false);


        ROS_INFO("TOFCamDriver using camera_id: %d", cam_id);

        tof_cam_param_.camera_config.cam_format = (TOF::CameraFormat)format;
        tof_cam_param_.camera_config.cam_id = cam_id;
        tof_cam_param_.camera_config.cam_type = (TOF::CameraType)cam_type;
        tof_cam_param_.camera_config.pixel_width = TOF_IRS_WIDTH;
        tof_cam_param_.camera_config.pixel_height = TOF_IRS_HEIGHT;
        tof_cam_param_.camera_config.memory_stride = TOF_IRS_WIDTH;
        tof_cam_param_.camera_config.fps = frame_rate;

        image_size_bytes_ = tof_cam_param_.camera_config.pixel_height *
            tof_cam_param_.camera_config.memory_stride *1.5;

        int ret = camera::ICameraDevice::createInstance(cam_id, &camera_ptr_);
        if(ret != 0){
            ROS_ERROR("ERROR: Could not open camera %d\n", cam_id);
            return ret;
        }
        else{
            printf("Opened camera %d Type: %d\n", cam_id, tof_cam_param_.camera_config.cam_type );
        }

        camera_ptr_->addListener(this);

        ret = params_.init(camera_ptr_);
        if(ret != 0){
            ROS_ERROR("ERROR: Failed to initialize camera parameters.\n");
            camera::ICameraDevice::deleteInstance(&camera_ptr_);
            return ret;
        }

        ROS_INFO("Setting output = RAW_FORMAT for tof camera sensor, set format BLOB\n");
        params_.setPreviewFormat(camera::FORMAT_BLOB);
        params_.set("preview-format", "bayer-mipi-12bggr");
        params_.set("raw-size", "224x172");

        enum TOFDataType
        {
                 LISTENER_NONE   = 0x0,
                 LISTENER_DEPTH_DATA = 0x1,
                 LISTENER_SPARSE_POINT_CLOUD = 0x2,
                 LISTENER_DEPTH_IMAGE = 0x4,         // 16 bit
                 LISTENER_IR_IMAGE = 0x8             // 8 bit
        };

        if(ir_image_mode_)
            params_.setDataOutput(LISTENER_IR_IMAGE);
        else
            params_.setDataOutput(LISTENER_DEPTH_DATA);

        int rc;
        rc = params_.commit();
        if(rc){
            ROS_ERROR("Committing data output type failed, exiting");
            exit(-1);
        }

        bool ok = loadLensParamsFromFile(lensParameters_);
        if(!ok){
            ROS_FATAL("ERROR: Failed to initialize camera parameters, fatal, unloading.\n");
            camera_ptr_->removeListener( this );
            camera::ICameraDevice::deleteInstance(&camera_ptr_);
            camera_ptr_ = nullptr;
            return -1;
        }

        prepRosMessages();
        advertiseTopics();
    }

    initialized_ = true;
    return 0;
}

int32_t TOF::CameraManager::setPostStartParams()
{
    if(initialized_){
        if(running_){
            ROS_INFO("Setting distance range to 9");
            params_.setDistanceRange(9);

            ROS_INFO_STREAM("Setting frame rate to: " << tof_cam_param_.camera_config.fps);
            const camera::Range frameRate(tof_cam_param_.camera_config.fps,
                                          tof_cam_param_.camera_config.fps, 0);
            params_.setPreviewFpsRange(frameRate);

            ROS_INFO_STREAM("Setting exposure mode to manual");
            params_.setExposureMode(auto_exposure_); // 0 = Manual, 1 = Auto

            int rc = params_.commit();
            if(rc){
                ROS_ERROR("commit failed");
                return -3;
            }
            else{
                ROS_INFO("commit success");
                return 0;
            }
        }
        else{
            ROS_WARN( "Tried to set params; CameraManager is not running." );
            return -2;
        }
    }
    else{
        ROS_ERROR( "Tried to set params; CameraManager has not yet been initialized.");
        return -1;
    }
}

int32_t TOF::CameraManager::Terminate()
{
    if( camera_ptr_ != nullptr ){
        //stop the camera.
        if(running_){
            Stop();
        }
        // remove this as a listener.
        camera_ptr_->removeListener( this );
        //delete the camera ptr
        camera::ICameraDevice::deleteInstance(&camera_ptr_);
        camera_ptr_ = nullptr;
    }
    initialized_ = false;
    return 0;
}

void TOF::CameraManager::GetMonotonicClockOffset()
{
    // Camera Frames are timestamped with the monotonic clock
    // ROS timestamps use the realtime clock.  Here we compute the difference
    // and apply to messages
    struct timespec time_monotonic;
    struct timespec time_realtime;
    clock_gettime(CLOCK_REALTIME, &time_realtime);
    clock_gettime(CLOCK_MONOTONIC, &time_monotonic);
    ros::Time realtime(time_realtime.tv_sec, time_realtime.tv_nsec);
    ros::Time monotonic(time_monotonic.tv_sec, time_monotonic.tv_nsec);
    monotonic_offset_ = realtime - monotonic;
}

int32_t TOF::CameraManager::Start()
{
    monotonic_offset_ = ros::Duration(0.0);
    GetMonotonicClockOffset();
    if(initialized_){
        if(!running_){
            int ret = camera_ptr_->startPreview();
            if(ret == 0){
                running_ = true;
                setPostStartParams();
                recon_server_.setCallback(boost::bind(&TOF::CameraManager::reconCallback, this, _1, _2));
            }
            else return ret;
        }
        else ROS_WARN( "Tried to start; CameraManager is already started." );
    }
    else{
        ROS_ERROR( "Tried to start; CameraManager has not yet been initialized.");
        return -1;
    }
    return 0;
}

int32_t TOF::CameraManager::Stop()
{
    if( running_ ){
        camera_ptr_->stopPreview();
    }
    running_ = false;
    return 0;
}


void TOF::CameraManager::onError()
{
    ROS_ERROR("libCamera onError");
    if( running_ ){
        camera_ptr_->stopPreview();
    }
    running_ = false;
}

void TOF::CameraManager::onPreviewFrame(camera::ICameraFrame* frame)
{
    std::lock_guard<std::mutex> lock(frame_mutex_);

    if(frame->timeStamp != timestamp_last_nsecs_){
        timestamp_last_nsecs_ = frame->timeStamp;
        image_size_bytes_ = frame->size;
        next_frame_id_++;

        ros::Time timestamp(frame->timeStamp / 1000000000, frame->timeStamp % 1000000000);
        timestamp += monotonic_offset_;
        timestamp += ros::Duration(time_offset_s_);

        if(ir_image_mode_)
            publishIrImage((uint8_t*)frame->data, frame->size, timestamp);
        else
            publishFrameMessages((uint8_t*)frame->data, frame->size, timestamp);
    }
    else{
        ROS_WARN( "Got duplicate image frame at timestamp: %lld frame_id: %lld\n",
                            frame->timeStamp, next_frame_id_ );
    }
    return;
}

void TOF::CameraManager::onVideoFrame(camera::ICameraFrame* frame)
{
    // Should only get here if video is requested, which is not supported
    ROS_WARN("Got video frame (we should not)!");
    return;
}

void TOF::CameraManager::prepRosMessages()
{
    std::string frame_id;
    pnh_.param("tof_depth_frame", frame_id, std::string("tof_camera"));

    ROS_INFO_STREAM("TOF tf frame: " << frame_id);

    prepCameraInfoMsg(TOF_IRS_HEIGHT, TOF_IRS_WIDTH, frame_id);
    prepDepthMsg(TOF_IRS_HEIGHT, TOF_IRS_WIDTH, frame_id);
    prepIrImageMsg(TOF_IRS_HEIGHT, TOF_IRS_WIDTH, frame_id);
    prepPointCloudMsg(TOF_IRS_HEIGHT, TOF_IRS_WIDTH, frame_id);
    prepLaserScanMsg(std::string("laser_scan"));
    return;
}

void TOF::CameraManager::prepCameraInfoMsg(int imageHeight, int imageWidth, std::string frame_id)
{
    // load lens params from EEPROM
    double fx = lensParameters_.focalLength.first;
    double fy = lensParameters_.focalLength.second;
    double rad0 = lensParameters_.distortionRadial[0];
    double rad1 = lensParameters_.distortionRadial[1];
    double rad2 = lensParameters_.distortionRadial[2];

    // when flipping the image, tangential distortion must be flipped
    // and primciple point reflected over the center of the image
    double px, py, tan0, tan1;
    if(flip_image_){
        px = TOF_IRS_WIDTH-lensParameters_.principalPoint.first;
        py = TOF_IRS_HEIGHT-lensParameters_.principalPoint.second;
        tan0 = -lensParameters_.distortionTangential.first;
        tan1 = -lensParameters_.distortionTangential.second;
    }
    else{
        px = lensParameters_.principalPoint.first;
        py = lensParameters_.principalPoint.second;
        tan0 = lensParameters_.distortionTangential.first;
        tan1 = lensParameters_.distortionTangential.second;
    }

    // populate cameraInfoMsg for ROS
    cameraInfoMsg_.header.frame_id = frame_id;
    cameraInfoMsg_.width = imageWidth;
    cameraInfoMsg_.height = imageHeight;

    // distortion parameters. 5-param radtan model identical to factory sensor cal
    cameraInfoMsg_.distortion_model = "plumb_bob";
    cameraInfoMsg_.D.resize(5);
    cameraInfoMsg_.D[0] = rad0;
    cameraInfoMsg_.D[1] = rad1;
    cameraInfoMsg_.D[2] = tan0;
    cameraInfoMsg_.D[3] = tan1;
    cameraInfoMsg_.D[4] = rad2;

    // Intrinsic camera matrix for the raw (distorted) images.
    cameraInfoMsg_.K[0] = fx;
    cameraInfoMsg_.K[1] = 0;
    cameraInfoMsg_.K[2] = px;
    cameraInfoMsg_.K[3] = 0;
    cameraInfoMsg_.K[4] = fy;
    cameraInfoMsg_.K[5] = py;
    cameraInfoMsg_.K[6] = 0;
    cameraInfoMsg_.K[7] = 0;
    cameraInfoMsg_.K[8] = 1;

    // Rectification matrix STEREO CAM ONLY, leave as identity for monocular
    cameraInfoMsg_.R[0] = 1;
    cameraInfoMsg_.R[1] = 0;
    cameraInfoMsg_.R[2] = 0;
    cameraInfoMsg_.R[3] = 0;
    cameraInfoMsg_.R[4] = 1;
    cameraInfoMsg_.R[5] = 0;
    cameraInfoMsg_.R[6] = 0;
    cameraInfoMsg_.R[7] = 0;
    cameraInfoMsg_.R[8] = 1;

    // Projection matrix
    cameraInfoMsg_.P[0] = fx;
    cameraInfoMsg_.P[1] = 0;
    cameraInfoMsg_.P[2] = px;
    cameraInfoMsg_.P[3] = 0;  // Tx for stereo offset
    cameraInfoMsg_.P[4] = 0;
    cameraInfoMsg_.P[5] = fy;
    cameraInfoMsg_.P[6] = py;
    cameraInfoMsg_.P[7] = 0;  // Ty for stereo offset
    cameraInfoMsg_.P[8] = 0;
    cameraInfoMsg_.P[9] = 0;
    cameraInfoMsg_.P[10] = 1;
    cameraInfoMsg_.P[11] = 0;

    return;
}

void TOF::CameraManager::prepDepthMsg(int imageHeight, int imageWidth, std::string frame_id)
{
    depthImageMsg_.header.frame_id = frame_id;
    depthImageMsg_.width = imageWidth;
    depthImageMsg_.height = imageHeight;
    depthImageMsg_.is_bigendian = false;
    depthImageMsg_.encoding = sensor_msgs::image_encodings::TYPE_16UC1;
    return;
}

void TOF::CameraManager::prepIrImageMsg(int imageHeight, int imageWidth, std::string frame_id)
{
    irImageMsg_.header.frame_id = frame_id;
    irImageMsg_.width = imageWidth;
    irImageMsg_.height = imageHeight;
    irImageMsg_.is_bigendian = false;
    irImageMsg_.encoding = sensor_msgs::image_encodings::MONO8;
    return;
}

void TOF::CameraManager::prepPointCloudMsg(int imageHeight, int imageWidth, std::string frame_id)
{
    pointCloudMsg_.header.frame_id = frame_id;
    pointCloudMsg_.width = imageWidth;
    pointCloudMsg_.height = imageHeight;

    pointCloudMsg_.is_bigendian = false;
    // Data is not "dense" since not all points are valid.
    pointCloudMsg_.is_dense = false;

    // The total size of the channels of the point cloud for a single point are:
    // sizeof(float) * 3 = x, y, and z
    // sizeof(float) = noise
    // sizeof(uint32_t) = depthConfidence (only the low byte is defined, using uin32_t for alignment)
    pointCloudMsg_.point_step = sizeof(float) * 3 + sizeof(float) + sizeof(uint32_t) + sizeof(uint16_t);
    pointCloudMsg_.row_step = 1;

    // Describe the fields (channels) associated with each point.
    pointCloudMsg_.fields.resize(NUM_POINT_CLOUD_CHANNELS);
    pointCloudMsg_.fields[0].name = "x";
    pointCloudMsg_.fields[1].name = "y";
    pointCloudMsg_.fields[2].name = "z";
    pointCloudMsg_.fields[3].name = "noise";

    // Defines the format of x, y, z, and noise channels.
    size_t index;
    for (index = 0; index < 4; index++)
    {
        pointCloudMsg_.fields[index].offset = sizeof(float) * index;
        pointCloudMsg_.fields[index].datatype = sensor_msgs::PointField::FLOAT32;
        pointCloudMsg_.fields[index].count = 1;
    }
    // Defines the format of the depthConfidence channel.
    pointCloudMsg_.fields[index].name = "confidence";
    pointCloudMsg_.fields[index].offset = pointCloudMsg_.fields[index - 1].offset + sizeof(float);
    pointCloudMsg_.fields[index].datatype = sensor_msgs::PointField::UINT32;
    pointCloudMsg_.fields[index].count = 1;

    index++;
    pointCloudMsg_.fields[index].name = "intensity";
    pointCloudMsg_.fields[index].offset = pointCloudMsg_.fields[index - 1].offset + sizeof(uint32_t);
    pointCloudMsg_.fields[index].datatype = sensor_msgs::PointField::UINT16;
    pointCloudMsg_.fields[index].count = 1;
    return;
}

void TOF::CameraManager::prepLaserScanMsg(std::string frame_id)
{
    laserScanMsg_.header.frame_id = frame_id;

    // scan_width_degrees is a ros argument
    // 96 for wide Sunny lens, 62 for narrow Infineon lens
    float scan_angle_rad = (scan_width_degrees_ * M_PI) / 180.0;
    float start_angle = 0.0;
    laserScanMsg_.angle_min = start_angle - (scan_angle_rad / 2.0);
    laserScanMsg_.angle_max = start_angle + (scan_angle_rad / 2.0);
    laserScanMsg_.angle_increment = scan_angle_rad / ((float)TOF_IRS_WIDTH);
    laserScanMsg_.time_increment = 0.0;
    laserScanMsg_.scan_time = 0.3333;
    laserScanMsg_.range_min = 0.0001;
    laserScanMsg_.range_max = 25.0;
    return;
}

void TOF::CameraManager::reconCallback(tof_cam_ros::TofCamConfig &config, uint32_t level)
{
    std::lock_guard<std::mutex> lock(frame_mutex_);

    if(config.exposure_time != config_.exposure_time)
    {
        if(auto_exposure_ == 0)
        {
            ROS_INFO_STREAM("Setting exposure time to: " << config.exposure_time);
            int64_t exp_time = config.exposure_time;
            params_.setExposureTime(exp_time);

            int rc = params_.commit();
            if(rc){
                ROS_ERROR("commit failed");
            }
            else{
                ROS_INFO("commit success");
            }
        }
        else
        {
            ROS_ERROR("Tried to set exposure time, but mode is set to auto exposure");
        }
    }
    config_ = config;
    return;
}

int TOF::CameraManager::publishIrImage(uint8_t* depthData, int depthSize, ros::Time frame_stamp)
{
    irImageMsg_.header.stamp = frame_stamp;
    cameraInfoMsg_.header.stamp = frame_stamp;

    royale::IRImage * ir_data = (royale::IRImage *)depthData;

    irImageMsg_.width   = ir_data->width;
    irImageMsg_.height  = ir_data->height;
    irImageMsg_.step    = ir_data->width;

    uint32_t total_bytes = irImageMsg_.height * irImageMsg_.step;
    irImageMsg_.data.resize(total_bytes);

    // 14 bytes for the width, height, streamid, and timestamp
    memcpy(&irImageMsg_.data[0], &depthData[14], total_bytes);
    // ^ Would prefer to use the ir_data->data.data(), but the royale::Vector
    // appears to be ill packed (data.size() does not return a consistent number)

    irImageMsg_.is_bigendian = false;
    ir_pub_.publish(irImageMsg_, cameraInfoMsg_);
    return 0;
}

int TOF::CameraManager::publishFrameMessages(uint8_t* depthData, int depthSize, ros::Time frame_stamp)
{
        struct royale::DepthData data;
        int size_points;

        memcpy((uint8_t*)&data.width, depthData, sizeof (data.width));
        depthData += sizeof (data.width);

        memcpy((uint8_t*)&data.height, depthData, sizeof (data.height));
        depthData += sizeof (data.height);

        memcpy((uint8_t*)&size_points, depthData, sizeof(size_points));
        depthData += sizeof(size_points);

        // Create a local pointing to the source and destination depth points
        // for readability.
        int point_count = size_points/sizeof(royale::DepthPoint);
        const royale::Vector<royale::DepthPoint> pointIn(point_count);

        memcpy((uint8_t*)&pointIn[0], depthData, size_points);

        depthImageMsg_.header.stamp = frame_stamp;
        pointCloudMsg_.header.stamp = frame_stamp;
        cameraInfoMsg_.header.stamp = frame_stamp;

        if(publish_ir_image_){
            irImageMsg_.header.stamp = frame_stamp;
            irImageMsg_.width   = data.width;
            irImageMsg_.height  = data.height;
            irImageMsg_.step    = data.width;
            irImageMsg_.data.resize(data.width * data.height);
        }

        // Define the dimensions of this point cloud based on the number data points received.
        pointCloudMsg_.width    = data.width;
        pointCloudMsg_.height   = data.height;
        pointCloudMsg_.row_step = pointCloudMsg_.point_step * data.width;
        pointCloudMsg_.data.resize(pointCloudMsg_.height * pointCloudMsg_.row_step);

        // TODO: Get rid of magic numbers
        // Set up the Laser Scan message. Pre-populate all ranges to infinity.
        int row_offset = 223;
        laserScanMsg_.ranges.assign(224, std::numeric_limits<double>::infinity());

        // Define the dimensions of the depth image based on the number of points received.
        depthImageMsg_.width  = data.width;
        depthImageMsg_.height = data.height;
        depthImageMsg_.step   = depthImageMsg_.width * NUM_BYTES_PER_IMAGE_PIXEL;
        depthImageMsg_.data.resize(depthImageMsg_.height * depthImageMsg_.step);

        size_t numPoints = pointIn.size();

        for (size_t pointIndex = 0; pointIndex < numPoints; ++pointIndex){
            if(publish_depth_image_){
                int depthInMm = 0;
                //if Confidence level is too low, ignore this depth level.
                // using Z value is fine, ROS takes depth image as distance along
                // optical axis, not distance to object
                if(pointIn[pointIndex].depthConfidence > confidence_cutoff_ &&
                                            pointIn[pointIndex].z > min_depth_m_){
                    depthInMm = pointIn[pointIndex].z * 1000;
                }

                if(flip_image_){
                    memcpy(&depthImageMsg_.data[(numPoints - pointIndex - 1) * NUM_BYTES_PER_IMAGE_PIXEL],
                                 &depthInMm, NUM_BYTES_PER_IMAGE_PIXEL);
                }
                else{
                    memcpy(&depthImageMsg_.data[pointIndex * NUM_BYTES_PER_IMAGE_PIXEL],
                                 &depthInMm, NUM_BYTES_PER_IMAGE_PIXEL);
                }
            }

            if(publish_point_cloud_){
                float sensor_x, sensor_y, sensor_z;
                if(flip_image_)
                {
                    sensor_x = -pointIn[pointIndex].x;
                    sensor_y = -pointIn[pointIndex].y;
                }
                else
                {
                    sensor_x = pointIn[pointIndex].x;
                    sensor_y = pointIn[pointIndex].y;
                }
                sensor_z = pointIn[pointIndex].z;

                memcpy(&pointCloudMsg_.data[pointIndex * pointCloudMsg_.point_step +
                                                                        pointCloudMsg_.fields[0].offset], &sensor_x, sizeof(float));
                memcpy(&pointCloudMsg_.data[pointIndex * pointCloudMsg_.point_step +
                                                                        pointCloudMsg_.fields[1].offset], &sensor_y, sizeof(float));
                memcpy(&pointCloudMsg_.data[pointIndex * pointCloudMsg_.point_step +
                                                                        pointCloudMsg_.fields[2].offset], &sensor_z, sizeof(float));
                memcpy(&pointCloudMsg_.data[pointIndex * pointCloudMsg_.point_step +
                                                                        pointCloudMsg_.fields[3].offset],
                             &pointIn[pointIndex].noise, sizeof(float));
                uint32_t confidence = pointIn[pointIndex].depthConfidence;
                memcpy(&pointCloudMsg_.data[pointIndex * pointCloudMsg_.point_step +
                                                                        pointCloudMsg_.fields[4].offset],
                             &confidence, sizeof(uint32_t));

                memcpy(&pointCloudMsg_.data[pointIndex * pointCloudMsg_.point_step +
                                                                        pointCloudMsg_.fields[5].offset],
                             &pointIn[pointIndex].grayValue, sizeof(uint16_t));
            }

            if(publish_laser_scan_){
                laserScanMsg_.header.stamp = frame_stamp;
                int start_offset = TOF_IRS_WIDTH * laser_scan_line_;
                if((pointIndex >= start_offset) &&
                        (pointIndex < start_offset + TOF_IRS_WIDTH)){
                    float range = sqrt((pointIn[pointIndex].x * pointIn[pointIndex].x) +
                                                         (pointIn[pointIndex].z * pointIn[pointIndex].z));
                    //float range = pointIn[pointIndex].z;
                    if(range > laserScanMsg_.range_min)
                    {
                        laserScanMsg_.ranges[row_offset] = range;
                    }
                    row_offset--;
                }
            }

            if(publish_ir_image_){
                //MSB
                //irImageMsg_.data[pointIndex] = (pointIn[pointIndex].grayValue >> 8) & 0xff;

                //LSB
                //irImageMsg_.data[pointIndex] = (pointIn[pointIndex].grayValue >> 0) & 0xff;

                // Truncating 16 bit IR image to 8 bit.  Isn't using MSB.  Appears the IR mode
                // potentiallly using a nonlinear mapping.  This works for now but could potentially
                // be improved.
                if(pointIn[pointIndex].grayValue > 255)
                    irImageMsg_.data[pointIndex] = 255;
                else
                    irImageMsg_.data[pointIndex] = pointIn[pointIndex].grayValue;
            }
        }

        if(publish_depth_image_)
            cam_pub_.publish(depthImageMsg_, cameraInfoMsg_);

        if(publish_ir_image_)
            ir_pub_.publish(irImageMsg_, cameraInfoMsg_);

        if(publish_point_cloud_)
            pubPointCloud_.publish(pointCloudMsg_);

        if(publish_laser_scan_)
            pubLaserScan_.publish(laserScanMsg_);

        return 0;
}

/**
 * Load the TOF sensor lens parameters from a file created
 * by lower level glue code when the TOF sensor was initialized
 *
 * @param lensParameters : camera lens calibration data
 *
 * @return bool
 */
bool TOF::CameraManager::loadLensParamsFromFile(royale::LensParameters & lensParameters)
{
    ROS_DEBUG("TOF::CameraManager::loadLensParamsFromFile()");

    const char *lensParamsFilePath = "/data/misc/camera/irs10x0c_lens.cal";
    const char *requiredVersion="VERSION:1.0:VERSION";
    std::vector<float> v_float;
    std::ifstream ifs;
    int maxTries = 10;
    bool ok = false;
    std::string line;

    while (maxTries-- > 0)
    {
        ifs.open(lensParamsFilePath, std::ifstream::in);
        if( ifs.is_open() )
        {
            maxTries = 0;
        }
        else
        {
            usleep(1000000);
        }
    }
    if(!ifs.is_open())
    {
        ROS_INFO("Error: cannot open lens parameters file %s.",lensParamsFilePath );
    }
    else
    {
        ok = std::getline(ifs, line);
        if( !ok || strncmp(line.c_str(), requiredVersion, strlen(requiredVersion)) != 0)
        {
            ROS_INFO("Error: the first line of %s must be %s",lensParamsFilePath, requiredVersion);
        }
        else
        {
            while(std::getline(ifs, line))
            {
                if(line.length() > 0 && line[0] != '#')
                {
                    float num;
                    std::stringstream linestream(line);
                    while (linestream >> num)
                    {
                        v_float.push_back(num);
                    }
                }
            }
        }
        ifs.close();
    }


    ok = (v_float.size() >=8);
    if(ok)
    {
        ROS_INFO("Loading lens parameters from %s.",lensParamsFilePath );
        lensParameters_.principalPoint = royale::Pair<float, float>(v_float[0], v_float[1]);
        lensParameters_.focalLength = royale::Pair<float, float>(v_float[2], v_float[3]);
        lensParameters_.distortionTangential = royale::Pair<float, float>(v_float[4], v_float[5]);
        for (int i = 6; i < v_float.size(); i++)
        {
            lensParameters.distortionRadial.push_back(v_float[i]);
        }
    }

    ROS_INFO_STREAM(
        std::endl <<
        "cx/cy     " << lensParameters_.principalPoint.first << " " << lensParameters.principalPoint.second << std::endl
        << "fx/fy     " << lensParameters_.focalLength.first << " " << lensParameters.focalLength.second << std::endl
        << "tan coeff "
        << (lensParameters_.distortionTangential.first)
        << " " << (lensParameters_.distortionTangential.second) << std::endl
        << "rad coeff "
        << lensParameters_.distortionRadial.at (0)
        << " " << lensParameters_.distortionRadial.at (1)
        << " " << lensParameters_.distortionRadial.at (2) << std::endl);

     return ok;
}
