/****************************************************************************
 *   Copyright (c) 2017 Michael Shomin. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name ATLFlight nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#ifndef TOF_CAM_MANAGER_HPP_
#define TOF_CAM_MANAGER_HPP_

// global/system includes.
#include <iostream>
#include <thread>
#include <vector>
#include <condition_variable>
#include <mutex>
#include <cstdint>
#include <cstring>
#include <string>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <royale/LensParameters.hpp>
#include <royale/DepthData.hpp>
#include <royale/IRImage.hpp>

// camera API Headers for the TOF flight.
#include "camera.h"
#include "camera_parameters.h"

#include "TOFCameraTypes.hpp"

#include <ros/ros.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/LaserScan.h>
#include <image_transport/image_transport.h>
#include <image_transport/camera_publisher.h>

#include <dynamic_reconfigure/server.h>
#include <tof_cam_ros/TofCamConfig.h>


namespace TOF {
  class CameraManager;
}

static const int NUM_BYTES_PER_IMAGE_PIXEL = sizeof(uint16_t);

//Class to support the IRS16 Camera.
class TOF::CameraManager : public camera::ICameraListener
{
public:
  const int DEFAULT_DEPTH_IMAGE_QUEUE_SIZE = 10;
  const int DEFAULT_POINT_CLOUD_QUEUE_SIZE = 10;
  const int DEFAULT_LASER_SCAN_QUEUE_SIZE = 10;
  const int NUM_POINT_CLOUD_CHANNELS = 6;
  CameraManager(ros::NodeHandle nh, ros::NodeHandle pnh);

  /**
   * Destructor for the camera manager.
   * make sure to close the camera instance if it opened.
   **/
  ~CameraManager()
  {
    Terminate();
  }

  /**
   * This function should be called first.
   * The configuration passed in the ctor will be used for intializing the camera
   * @return int32_t
   *  0=success
   *  otherwise=failure.
   */
  int32_t Initialize();

  /**
   * Function to start the camera interface.
   * Note: Initialize Shall be called first.
   * @return int32_t
   *  0 = success;
   *  otherwise = failure.
   *  If Initialize is not called first, this function will fail.
   */
  int32_t Start();

  /**
   * Function to stop the camera.
   * @return int32_t
   *  0 = success;
   *  otherwise = failure.
   */
  int32_t Stop();

  /**
   * Function to allow for clean-up of resources allocated during Initalize method.
   * @return int32_t
   *  0 = success
   *  otherwise = failure.
   */
  int32_t Terminate();

  /**
   * Function to check if the camera manager is running
   * @return bool
   *   true = running
   *   false = not running
   */
  inline bool IsRunning() const
  {
    return running_;
  }

  /**
   * Function to get the image size reported by the camera
   * @returns size_t
   *   size of the camera image in bytes
   */
  inline size_t GetImageSize() const
  {
    return image_size_bytes_;
  }


 //interface functions from the camera::ICameraListener
  virtual void onError();
  virtual void onPreviewFrame(camera::ICameraFrame* frame);
  virtual void onVideoFrame(camera::ICameraFrame* frame);

private:
  void advertiseTopics();
  void GetMonotonicClockOffset();
  void prepRosMessages();
  void prepCameraInfoMsg(int imageHeight, int imageWidth, std::string frame_id);
  void prepDepthMsg(int imageHeight, int imageWidth, std::string frame_id);
  void prepIrImageMsg(int imageHeight, int imageWidth, std::string frame_id);
  void prepPointCloudMsg(int imageHeight, int imageWidth, std::string frame_id);
  void prepLaserScanMsg(std::string frame_id);
  int publishFrameMessages(uint8_t* depthData, int depthSize, ros::Time frame_stamp);
  int publishIrImage(uint8_t* depthData, int depthSize, ros::Time frame_stamp);
  bool loadLensParamsFromFile(royale::LensParameters & lensParameters);
  void reconCallback(tof_cam_ros::TofCamConfig &config, uint32_t level);
  int32_t setPostStartParams();

  bool initialized_;
  bool running_;
  bool preview_params_set_;

  bool publish_point_cloud_;
  bool publish_laser_scan_;
  bool publish_depth_image_;
  bool flip_image_;
  bool publish_ir_image_;
  bool ir_image_mode_;
  int auto_exposure_;
  int laser_scan_line_;
  int scan_width_degrees_;

  int confidence_cutoff_;
  float time_offset_s_;
  float min_depth_m_;

  TOF::CameraParameters tof_cam_param_;
  camera::ICameraDevice* camera_ptr_;
  camera::CameraParams   params_;
  camera::ImageSize      preview_size_;

  size_t   image_size_bytes_;
  uint64_t timestamp_last_nsecs_;
  int64_t  next_frame_id_;
  uint32_t frame_q_write_index_;
  uint32_t frame_q_read_index_;

  std::mutex              frame_mutex_;
  std::condition_variable frame_cv_;
  ros::Duration monotonic_offset_;

  royale::LensParameters lensParameters_;

  sensor_msgs::CameraInfo cameraInfoMsg_;
  sensor_msgs::Image depthImageMsg_;
  sensor_msgs::Image irImageMsg_;
  sensor_msgs::PointCloud2 pointCloudMsg_;
  sensor_msgs::LaserScan laserScanMsg_;

  ros::Publisher pubPointCloud_;
  ros::Publisher pubLaserScan_;

  ros::NodeHandle nh_;
  ros::NodeHandle pnh_;

  dynamic_reconfigure::Server<tof_cam_ros::TofCamConfig> recon_server_;
  tof_cam_ros::TofCamConfig config_;

protected:
  image_transport::ImageTransport im_trans_;
  image_transport::CameraPublisher cam_pub_;
  image_transport::CameraPublisher ir_pub_;
};

#endif //TOF_CAM_MANAGER_HPP_
